"""Contains a class to study sequences of states."""
import sys

import numpy as np
import matplotlib.pyplot as plt

sys.path.append("../utilities")
from plotting_utils import plot_id_checker

class SequenceWorkerLib:
    """ A class to represent a sequence of states.
    
    Attributes
    ----------
        seq : 1d array of int
            sequence of int representing states.
        states : 1d array of int
            list of individual states.
        n_states : int
            number of individual states.
        n_frames : int
            number of frames in the sequence.
        p : 1d array floats
            probability of finding each state.
        T : 2d array floats
            transition probability matrix between states.
        mc : 1d array of ints
            sequence of int representing states, generated from Markov Chain.
            Note : only exists after the :markov_chain: method is called.
        comes_from : str
            explaining how data was provided to the class.
                if provided as an array : 'parsed array'
                if provided as a path : the path
                
    Methods
    -------
        markov_chain(N=10000, start=None):
            generate a sequence of states from a Markov Chain.
        evaluate_markov():
            produce a plot comparing original sequence statistics with
            statistics of MC generated sequence.
            Note : the :markov_chain: method needs to be run beforehand.
            
    Class Methods
    -------------
        _prior(seq, n_states=None):
            compute the probability to see each state from a state sequence.
        _transition_matrix(seq, n_states=None, *, tau=1):
            compute the transition probability matrix from a state sequence.
            
    Magic Methods
    -------------
        __eq__:
            Compare 2 instances of this class with the == operator.
            return a graph of the statistics comparaison, and a bool.
    """
    def __init__(self, seq, dt=0.1):
        """Initialize from a sequence.
        
        Parameters
        ----------
        seq : 1d array of int or path to .npy file
            sequence of int representing states.
        dt : float, optional
            duration of each step in seconds.
        """
        # handling :seq:
        if isinstance(seq, np.ndarray):
            self.seq = sequence
            #self.seq = seq
            self.comes_from = "parsed array"
        elif isinstance(seq, str) and (seq[-4:] == ".npy"):
            self.seq = np.load(seq)
            self.comes_from = seq
        else:
            raise TypeError(
                ":seq: is not a recognised variable." 
                "Should be a numpy array or a path"
            )
        assert self.seq.ndim == 1, ":seq: should be 1d"
        self.states = np.unique(self.seq)
        self.n_states = len(self.states)
        self.n_frames = len(self.seq)
            
        self.dt = dt
        
        self.p = self._prior(self.seq, n_states=self.n_states)
        self.T = self._transition_matrix(self.seq, n_states=self.n_states)
        
    def __eq__(self, other):
        """Compare 2 instances of this class with the == operator.
        
        Returns
        -------
        a graph of the statistics comparaison.
        a bool :
            True if both the prior and the transition matrices are comparable.
            False if at least one of them is not comparable.
        """
        fig, axs = plt.subplots(ncols=2, figsize=(2*2.5, 2))
        fig.suptitle("Comparing 2 sequences")

        plot_id_checker(self.T, other.T, ax=axs[0], labels=["Sequence 1 (self)", "Sequence 2 (other)"])
        axs[0].set_title(r"$P(x_{t+1} = j | x_t=i)$")

        plot_id_checker(self.p, other.p, ax=axs[1], labels=["Sequence 1 (self)", "Sequence 2 (other)"])
        axs[1].set_title(r"$P(x=i)$")
        
        cond_1 = np.isclose(self.T, other.T, rtol=1.e-2, atol=1.e-2).all()
        cond_2 = np.isclose(self.p, other.p, rtol=1.e-2, atol=1.e-2).all()
        return (cond_1 and cond_2)
    
    def markov_chain(self, N=10000, start=None):
        """Generate a sequence of states using a Markov Chain.
        
        Parameters
        ----------
        N : int, optional
            the number of sample to perform.
        start : None or str or int, optional
            the starting point of the chain.
            if None : start from most probable transited state.
            if "rand" : start for a random state.
            if "prior" : start from the most probable state.
            if int : start from the given state.
            
        Note
        ----
        The output of the markov chain is stored in the attribute :mc: as an
        array of ints.
        """
        self.mc = np.empty(N,dtype=np.int_)
        if start is None:
            self.mc[0] = np.argmax(self.T.sum(axis=0))
        elif start == "rand":
            self.mc[0] = np.random.choice(self.states)
        elif start == "prior":
            self.mc[0] = np.random.choice(self.states, p=self.p)
        else:
            self.mc[0] = start

        for i in range(1,N):
            self.mc[i] = np.random.choice(self.states, p=self.T[self.mc[i-1]])
            
    def evaluate_markov(self):
        """Compare original sequence stats with MC generated stats.
        
        Produce a plot comparing original sequence statistics with statistics
        of MC generated sequence.
        
        Returns
        -------
        fig : matplotlib.figure.Figure instance   
            
        Note
        ----
        the :markov_chain: method needs to be run beforehand.
        """
        p_gen = self._prior(self.mc, n_states=self.n_states)
        T_gen = self._transition_matrix(self.mc, n_states=self.n_states)
        
        fig, axs = plt.subplots(ncols=2, figsize=(2*2.5, 2))
        fig.suptitle("Evaluating Markov Chain")

        plot_id_checker(self.T, T_gen, ax=axs[0])
        axs[0].set_title(r"$P(x_{t+1} = j | x_t=i)$")

        plot_id_checker(self.p, p_gen, ax=axs[1])
        axs[1].set_title(r"$P(x=i)$")
        
        return fig
        
    @classmethod
    def _prior(cls, seq, n_states=None):
        '''Compute the probability of finding each state in a sequence

        Parameters
        ----------
        seq : 1d array of int
            sequence of int representing states.
        n_states : int, optional
            number of possible states. If not provided, it will be compute 
            directly from :seq:. This is the default behaviour.

        Returns
        -------
        T : 2d array of float (n_states, n_states)
            the transition probabilities P_i = P(x_j=a) .
        '''
        assert seq.ndim == 1, ":seq: should be 1d"

        if n_states is None:
            _, freq = np.unique(seq, return_counts=True)
        else:
            freq, bins = np.histogram(seq, bins=n_states)
        return freq / seq.shape[0]
    
    @classmethod
    def _transition_matrix(cls, seq, n_states=None, *, tau=1):
        '''Compute the transition matrix from a sequence of states.

        Parameters
        ----------
        seq : 1d array of int
            sequence of int representing states.
        n_states : int, optional
            number of possible states. If not provided, it will be compute 
            directly from :seq:. This is the default behaviour.
        tau : int, optional
            compute the :tau:-step transition probabilities.
            i.e. P(x_j=a | x_i=b, j=i+tau). Default is 1.

        Returns
        -------
        T : 2d array of float (n_states, n_states)
            the transition probabilities P_ij = P(x_j=a | x_i=b, j=i+tau), with 
            normalization sum_j P_ij = 1 .
        '''
        assert seq.ndim == 1, ":seq: should be 1d"
        if n_states is None:
            n_states = len(np.unique(seq))

        n_transitions = len(seq) - 1
        T = np.zeros((n_states,n_states))
        for i in range(n_transitions):
            k,l = seq[i], seq[i+1]
            T[k,l] += 1
        T = T / T.sum(axis=1)[:,np.newaxis]
        return T