"""Collection of useful plotting functions."""

import matplotlib.pyplot as plt

def plot_id_checker(
    X,Y, 
    ax=None, 
    labels=["Original","Generated"],
    log=True,
    s=1,
    color="k",
    **scatter_kwargs,
):
    '''Scatter plot to check that 2 arrays have similar values.
    
    Parameters
    ----------
    X,Y : two arrays of same shape
        Two arrays of values to compare. They will both be flattended.
    ax : matplotlib.axes._subplots.AxesSubplot, optional
        the ax on which to plot. If not provided, a new figure will be created
    labels : sequence of 2 strings, optional
        labels of the x and y axes
    log : bool, optional
        whether both axes should be in log scale
    s : int, optional
        size of markers (as defined in matplotlib.pyplot.scatter).
    color : valid matplotlib color, optional
        color of markers (as defined in matplotlib.pyplot.scatter).
    
    Other Parameters
    ----------------
    **scatter_kwargs : keyword parameters parsed to matplotlib.pyplot.scatter.
        
    Returns
    -------
    ax : matplotlib.axes._subplots.AxesSubplot
        the axe on which the plot was made.
    '''
    if ax is None:
        fig, ax = plt.subplots()
        
    # flattening the data
    X = X.ravel()
    Y = Y.ravel()
    assert X.shape == Y.shape, "X and Y should have the same size"
        
    # setting up the ax
    ax.set_xlabel(labels[0])
    ax.set_ylabel(labels[1])
    if log:
        ax.set_yscale('log')
        ax.set_xscale('log')
    ax.set_aspect("equal")
        
    # plotting
    mmin, mmax = min(X.min(), Y.min()), max(X.max(), Y.max())
    ax.plot([mmin, mmax], [mmin, mmax], color="k", alpha=0.5)
    ax.scatter(X, Y, s=s, color=color, **scatter_kwargs)
    
    return ax